<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJavascriptBlockFromPages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pages', function (Blueprint $table) {
            $table->text('javascript')->nullable();
            $table->text('header_javascript')->nullable();
            $table->text('end_javascript')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pages', function (Blueprint $table) {
            $table->dropColumn('javascript');
            $table->dropColumn('header_javascript');
            $table->dropColumn('end_javascript');
        });
    }
}
