<?php

namespace App\Console\Commands;

use Backpack\Settings\app\Models\Setting;
use Illuminate\Console\Command;

class CreateWebsiteSettings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'settings:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create the necessary settings.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $settings = [
            [
                'key' => 'website_name',
                'name' => 'Site name',
                'description' => 'The name of the website',
                'value' => 'Simple Blog',
                'field' => '{"name":"value","label":"Styles","type":"text"}',
                'active' => '1',
            ],
            [
                'key' => 'css',
                'name' => 'Styles',
                'description' => 'Css for the website',
                'value' => null,
                'field' => '{"name":"value","label":"Styles","type":"textarea"}',
                'active' => '1',
            ]
//            [
//                'key' => null,
//                'name' => null,
//                'description' => null,
//                'value' => null,
//                'field' => null,
//                'active' => null,
//            ],
        ];
        foreach ($settings as $setting) {
            $this->info('Create setting: '.$setting['name']);
            $newSetting = Setting::where('key', $setting['key'])->firstOrNew();
            $newSetting->key = $setting['key'];
            $newSetting->name = $setting['name'];
            $newSetting->description = $setting['description'];
            $newSetting->field = $setting['field'];
            $newSetting->active = $setting['active'];
            if(!$newSetting->exists()) {
                $newSetting->value = $setting['value'];
            }
            $newSetting->save();
            $this->info('Done');
        }
        return 0;
    }
}
