<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PageRequest;
use App\Models\Page;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PageCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PageCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;
    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Page::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/page');
        CRUD::setEntityNameStrings('page', 'pages');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('title');
        CRUD::column('status')->type('select_from_array')->options([
            Page::STATUS_DRAFT => 'Brouillon',
            Page::STATUS_PUBLISHED => 'Publié',
        ]);
        # CRUD::column('content');
        CRUD::column('show_photos')->label('Afficher les photos');;
        CRUD::column('is_homepage')->label('Page d\'accueil');;
        CRUD::column('created_at');
        # CRUD::column('uuid');
        # CRUD::column('parent_id');
        # CRUD::column('lft');
        # CRUD::column('rgt');
        # CRUD::column('depth');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PageRequest::class);

        CRUD::field('title');
        CRUD::field('status')->type('select_from_array')->options([
            Page::STATUS_DRAFT => 'Brouillon',
            Page::STATUS_PUBLISHED => 'Publié',
        ]);
        CRUD::field('meta_description')->type('textarea');
        CRUD::field('meta_tags')->type('textarea');
        CRUD::field('is_homepage')->label('Page d\'accueil');
        CRUD::field('content')
            ->type('summernote')
            ->options([
                'height' => 300,
                'maximumImageFileSize' => 70000,
            ]);
        CRUD::field('show_photos')->label('Afficher les photos');
        CRUD::field('images')->type('select_multiple');
        CRUD::field('header_javascript')->type('textarea');
        CRUD::field('javascript')->type('textarea')->options([
            'title' => "Body javascript",
        ]);
        CRUD::field('end_javascript')->type('textarea');
        # CRUD::field('uuid');
        # CRUD::field('parent_id');
        # CRUD::field('lft');
        # CRUD::field('rgt');
        # CRUD::field('depth');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupReorderOperation()
    {
        // define which model attribute will be shown on draggable elements
        $this->crud->set('reorder.label', 'title');
        // define how deep the admin is allowed to nest the items
        // for infinite levels, set it to 0
        $this->crud->set('reorder.max_level', 1);
    }
}
