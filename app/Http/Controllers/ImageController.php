<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ImageController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function __invoke(Request $request, $imageUuid)
    {
        $image = \App\Models\Image::where('uuid', $imageUuid)->first();
        if ($request->has('size')) {
            $imagePath = $image->getImagePath();
            $fileExtension = pathinfo($imagePath, PATHINFO_EXTENSION);

            $imagePath = match ($request->size) {
                'low' => $imagePath . '.100x100.' . $fileExtension,
                'medium' => $imagePath . '.500x500.' . $fileExtension,
                'large' => $imagePath . '.1024x1024.' . $fileExtension,
            };
            return response()->file($imagePath);
        }
        return response()->file($image->getImagePath());
    }
}
