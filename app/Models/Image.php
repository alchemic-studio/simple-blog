<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image as InterventionImage;
use Spatie\ImageOptimizer\OptimizerChainFactory;

/**
 * App\Models\Image
 *
 * @property int $id
 * @property string $path
 * @property string $filename
 * @property string $mime_type
 * @property string|null $name
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Image newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Image newQuery()
 * @method static \Illuminate\Database\Query\Builder|Image onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Image query()
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereFilename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereMimeType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Image withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Image withoutTrashed()
 * @mixin \Eloquent
 */
class Image extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory, SoftDeletes;

    protected $disk = 'public';

    protected $fillable = [
        'path',
        'filename',
        'mime_type',
        'name',
        'description',
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($model) {
            $model->uuid = Str::uuid();
        });

        static::updating(function ($model) {
            Slug::updateSlug($model, $model->title);
        });

        static::deleting(function($obj) {
            \Storage::disk('public')->delete($obj->path);
        });
    }

    public function getImagePath(): string
    {
        return storage_path('app/' . $this->disk  . '/' . $this->path);
    }

    public function getNameAttribute(): string
    {
        return $this->name ?? $this->filename;
    }

    public function getLinkAttribute(): string
    {
        return route('image.view', $this->uuid);
    }

    public function getLinkLowAttribute(): string
    {
        return route('image.view', [$this->uuid, 'size' => 'low']);
    }

    public function getLinkMediumAttribute(): string
    {
        return route('image.view', [$this->uuid, 'size' => 'medium']);
    }

    public function getLinkLargeAttribute(): string
    {
        return route('image.view', [$this->uuid, 'size' => 'large']);
    }

    public function setPathAttribute($value)
    {
        $attribute_name = "path";
        $md5Sum = request()->file($attribute_name)->hashName();
        $destination_path = "images/" . substr($md5Sum, 0, 2) . "/" . substr($md5Sum, 2, 2);

        $this->uploadFileToDisk($value, $attribute_name, $this->disk, $destination_path);
        $this->filename = request()->file($attribute_name)->getClientOriginalName();

        $imagePath = $this->getImagePath();
        $this->mime_type = mime_content_type($imagePath);
        $this->size = filesize($imagePath);
        $this->hash = $md5Sum;

        $fileExtension = pathinfo($imagePath, PATHINFO_EXTENSION);
        $optimizerChain = OptimizerChainFactory::create();
        $optimizerChain->optimize($imagePath);
        InterventionImage::make($imagePath)->widen(100)->save($imagePath.'.100x100.'.$fileExtension);
        $optimizerChain->optimize($imagePath.'.100x100.'.$fileExtension);
        InterventionImage::make($imagePath)->widen(500)->save($imagePath.'.500x500.'.$fileExtension);
        $optimizerChain->optimize($imagePath.'.500x500.'.$fileExtension);
        InterventionImage::make($imagePath)->widen(1024)->save($imagePath.'.1024x1024.'.$fileExtension);
        $optimizerChain->optimize($imagePath.'.1024x1024.'.$fileExtension);
    }

    public function articles(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Article::class);
    }

    public function pages(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Page::class);
    }
}
