<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Slug
 *
 * @property int $id
 * @property string $slug
 * @property string $type
 * @property int $type_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Slug newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Slug newQuery()
 * @method static \Illuminate\Database\Query\Builder|Slug onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Slug query()
 * @method static \Illuminate\Database\Eloquent\Builder|Slug whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Slug whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Slug whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Slug whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Slug whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Slug whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Slug whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Slug withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Slug withoutTrashed()
 * @mixin \Eloquent
 */
class Slug extends Model
{
    use HasFactory, SoftDeletes;

    public static function updateSlug($model, $title)
    {
        $slug = Slug::where('type', get_class($model))
            ->where('type_id', $model->id)
            ->whereSlug(\Str::slug($title))
            ->first();
        if ($slug) {
            return $slug;
        }
        $slug = new Slug();
        $slug->type = get_class($model);
        $slug->type_id = $model->id;
        $slug->slug = \Str::slug($title);
        $slug->uuid = \Str::uuid();
        $slug->save();
        return $slug;
    }
}
