<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $page = \App\Models\Page::whereIsHomepage(true)->firstOrFail();
    if ($page->status === \App\Models\Page::STATUS_PUBLISHED) {
        return view('page', ['page' => $page]);
    } else {
        if (Auth('backpack')->user() !== null) {
            return view('page', ['page' => $page]);
        } else {
            return abort(404);
        }
    }
});
Route::get('/pages/{slug}', function ($slug) {
    $slugModel = \App\Models\Slug::whereType('App\Models\Page')->whereSlug($slug)->firstOrFail();
    $page = \App\Models\Page::findOrFail($slugModel->type_id);
    if ($page->status === \App\Models\Page::STATUS_PUBLISHED) {
        return view('page', ['page' => $page]);
    } else {
        if (Auth('backpack')->user() !== null) {
            return view('page', ['page' => $page]);
        } else {
            return abort(404);
        }
    }
})->name('page.show');
Route::get('/articles/{slug}', function ($slug) {
    $slugModel = \App\Models\Slug::whereType('App\Models\Article')->whereSlug($slug)->firstOrFail();
    $article = \App\Models\Article::findOrFail($slugModel->type_id);
    if ($article->status === \App\Models\Article::STATUS_PUBLISHED) {
        return view('article', ['article' => $article, 'page' => $article->page]);
    } else {
        if (Auth('backpack')->user() !== null) {
            return view('page', ['article' => $article, 'page' => $article->page]);
        } else {
            return abort(404);
        }
    }
})->name('article.show');

Route::get('/images/{uuid}', \App\Http\Controllers\ImageController::class)->name('image.view');
