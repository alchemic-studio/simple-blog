@if($model->show_photos)
<div class="row">
    @foreach($model->images()->get() as $_slide)
    <div class="col">
            <a data-bs-toggle="modal" href="#" data-bs-target="#modal-image-{{$loop->iteration}}">
                <img src="{{$_slide->link_medium}}" class="img-thumbnail" alt="{{$_slide->title}}">
            </a>
            <div class="modal fade" id="modal-image-{{$loop->iteration}}" tabindex="-1">
                <div class="modal-dialog modal-fullscreen">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="container">
                                <div class="row mb-2">
                                    <div class="col" style="position: relative;">
                                        <img src="{{$_slide->link}}" class="img-fluid" alt="{{$_slide->title}}">
                                        <div class="border rounded bg-light p-1 image-label">
                                            <span><strong>{{$_slide->name}}</strong>
                                            @if(isset($_slide->name) && isset($_slide->description)) <br> @endif
                                            {{$_slide->description}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        @if($loop->iteration > 1)
                                            <button class="btn btn-primary" data-bs-target="#modal-image-{{$loop->iteration - 1}}" data-bs-toggle="modal">
                                                Précédent
                                            </button>
                                        @endif
                                    </div>
                                    <div class="col">
                                        <div class="col text-center">
                                            <button class="btn btn-light-outline pull-right" data-bs-toggle="modal">
                                                Fermer
                                            </button>
                                    </div>
                                    </div>
                                    <div class="col text-end">
                                        @if($loop->iteration < $loop->count)
                                            <button class="btn btn-primary pull-right" data-bs-target="#modal-image-{{$loop->iteration + 1}}" data-bs-toggle="modal">
                                                Suivant
                                            </button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    @endforeach
</div>
@endif
