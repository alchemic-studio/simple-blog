<div class="row">
    <div class="col-12">
        <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="false">
            <div class="carousel-inner">
                @foreach($model->images()->get() as $_slide)
                    <div class="carousel-item @if($loop->first) active @endif">
                        <img src="{{$_slide->link}}" class="d-block w-100" alt="{{$_slide->title}}">
                        <div class="carousel-caption d-none d-md-block">
                            <h5>{{$_slide->title}}</h5>
                            <p>{{$_slide->description}}</p>
                        </div>
                    </div>
                @endforeach
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </div>
</div>
