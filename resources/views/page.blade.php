<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @if(isset($article))
        <meta name="description" content="{{$article->meta_description}}">
        <meta name="keywords" content="{{$article->meta_tags}}">
    @else
        {!! $page->header_javascript !!}
        <meta name="description" content="{{$page->meta_description}}">
        <meta name="keywords" content="{{$page->meta_tags}}">
    @endif
    <title>{{Setting::get('website_name')}} - {{$page->title}}</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <style>
        {!! Setting::get('css') !!}
        .image-label {
            position: absolute;
            bottom: 0;
            margin: 8px;
            opacity: 0.8;
            font-size: 0.9em;
        }
        .draft {
            opacity: 0.9;
            position: absolute;
            bottom: 0;
            right: 0;
            color: #000000;
            z-index: 99999999999999999;
        }
    </style>
</head>
<body>
@if((isset($article) && $article->status === \App\Models\Article::STATUS_DRAFT) || $page->status === \App\Models\Page::STATUS_DRAFT)
    <div class="draft border bg-success rounded p-3 m-3">
        <strong>DRAFT</strong>
    </div>
@endif
<nav class="navbar mb-2">
    <div class="container">
        <a class="navbar-brand" href="/">{{Setting::get('website_name')}}</a>
        <div id="navbarNavDropdown">
            <ul class="nav">
                @foreach(\App\Models\Page::orderBy('lft')->get()->all() as $_page)
                    @if($_page->status === \App\Models\Page::STATUS_PUBLISHED || Auth('backpack')->user() !== null)
                        <li class="nav-item">
                            <a class="nav-link" href="/pages/{{$_page->getSlug()}}">{{$_page->title}}</a>
                        </li>
                    @endif
                @endforeach
                <li class="nav-item">
                    <div id="ConstellationWidgetContainer9ec21ce2-dfdd-46a4-b672-ecd57cd9e8ad" style="width:100%" title="La petite Mazure" data-id-projet='9ec21ce2dfdd46a4b672ecd57cd9e8ad'>Chargement en cours...</div> <script type="text/javascript" src="https://reservation.elloha.com/Scripts/widget-loader.min.js?v=42"></script> <script type="text/javascript">var constellationWidgetUrl9ec21ce2dfdd46a4b672ecd57cd9e8ad, constellationTypeModule9ec21ce2dfdd46a4b672ecd57cd9e8ad; constellationWidgetUrl9ec21ce2dfdd46a4b672ecd57cd9e8ad = 'https://reservation.elloha.com/Widget/BookingEngine/9ec21ce2-dfdd-46a4-b672-ecd57cd9e8ad?idoi=e0dd0ead-68fc-4a9a-89a4-9da79779d240&culture=fr-FR'; constellationTypeModule9ec21ce2dfdd46a4b672ecd57cd9e8ad=1; constellationWidgetLoad('ConstellationWidgetContainer9ec21ce2-dfdd-46a4-b672-ecd57cd9e8ad'); constellationWidgetAddEvent(window, "resize", function () { constellationWidgetSetAppearance('ConstellationWidgetContainer9ec21ce2-dfdd-46a4-b672-ecd57cd9e8ad'); });</script>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container container-page-{{$page->id}} @if(isset($article))container-article-{{$article->id}}@endif">
    <div class="row">
        @if($page->articles()->get()->count() > 0)
            <div class="col-4 col-lg-2">
                <ul class="list-group">
                    @foreach($page->articles()->get()->all() as $_article)
                        @if($_article->status === \App\Models\Page::STATUS_PUBLISHED || Auth('backpack')->user() !== null)
                        <li class="list-group-item">
                            <a href="{{route('article.show', $_article->getSlug())}}">{{$_article->title}}</a>
                        </li>
                        @endif
                    @endforeach
                </ul>
            </div>
            <div class="col-8 col-lg-10">
        @else
        <div class="col-12">
        @endif
            <div class="row">
                <div class="col-12 content">
                    @if(isset($article))
                        {!! $article->content !!}
                    @else
                        {!! $page->javascript !!}
                        {!! $page->content !!}
                    @endif
                </div>
            </div>
            <x-thumbnails :model="isset($article) ? $article : $page" />
        </div>
    </div>
    <div class="row mt-3">
        <br>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
</body>
</html>
